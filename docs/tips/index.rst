
.. _lscsoft-conda-tips:

===============
Tips and tricks
===============

--------------------
Clone an environment
--------------------

You can clone one of the pre-built environments (or any environment, generally) with

.. code-block:: bash

   conde create --name <target> --clone <source>

e.g.:

.. code-block:: bash

   conda create --name myligo-py37 --clone ligo-py37

--------------------
Preserve your prompt
--------------------

By default, activating a conda environment will change your login prompt.
This behaviour can be disabled with

.. code-block:: bash

   conda config --set changeps1 no


-----------
Using LaTeX
-----------

The LSCSoft Conda environments do not provide (La)TeX.
You should install TeX using your system package manager, see below for
details for some common operating systems:

.. note::

   The following examples should get you far enough to use TeX with
   the Matplotlib Python library.  If you find that other packages
   are needed, please
   `open a Bug ticket <https://git.ligo.org/lscsoft/conda/issues/new?issuable_template=Bug>`_.

^^^^^^^^
Macports
^^^^^^^^

.. code-block:: bash

   port install texlive dvipng
