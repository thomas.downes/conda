#!/usr/bin/env python

"""Format the current list of environments for inclusion in the sphinx docs
"""

import json
import os.path
import pathlib
import re
from subprocess import check_output

import yaml

from sphinx.util import logging

from tabulate import tabulate

logger = logging.getLogger('utils')

ANACONDA_CHANNELS = {
    "pkgs/free",
    "pkgs/main",
}

DOCKER_IMAGE = os.getenv("LSCSOFT_CONDA_DOCKER_IMAGE", None)


def run_conda_command(cmd):
    if DOCKER_IMAGE:
        cmd = ["docker", "run", DOCKER_IMAGE, "ligo-py37"] + cmd
    logger.info("[utils] $ {0}".format(" ".join(cmd)))
    return check_output(cmd)


def find_environments(path=None):
    """Finds all of the environments known to conda
    """
    # get files
    if path is None:
        path = pathlib.Path(__file__).resolve().parent.parent
    files = path.glob('environment-*.yml')

    # get environments
    envs = json.loads(run_conda_command([
        'conda',
        'env',
        'list',
        '--json',
        '--quiet',
    ]).strip().decode())['envs']
    env_names = [pathlib.Path(env).name for env in envs]

    # cross-check
    for fn in files:
        with fn.open("rb") as envf:
            content = yaml.load(envf)
            name = content['name']
            if name in env_names:
                yield fn


def write_environment(env, file=None):
    env = pathlib.Path(env)
    with env.open("rb") as envf:
        content = yaml.load(envf)

    # write name
    name = content['name']
    print("*" * len(name), file=file)
    print(name, file=file)
    print("*" * len(name), file=file)
    print("", file=file)

    # write link
    print("| **Download:** `{0} <https://git.ligo.org/lscsoft/conda/raw/master/"
          "{0}>`_".format(env.name),
          file=file,
    )
    channels = content['channels']
    if 'nodefaults' not in channels:
        channels.insert(0, 'defaults')
    print("| **Channels:** ``{0}``".format("``, ``".join(channels)),
          file=file)
    print("| **Install:**\n\n::\n\n"
          "   conda env create --file {0}\n".format(env.name),
          file=file)

    # get packages
    celist = run_conda_command([
        'conda',
        'list',
        '--name',
        name,
        '--json',
        '--quiet',
    ])
    packages = json.loads(celist)

    # write packages
    print("| **Packages:**\n", file=file)
    rows = []
    images = {}
    for pkg in packages:
        cloudchannel = ('anaconda' if pkg['channel'] in ANACONDA_CHANNELS else
                        pkg['channel'])
        rows.append((
            "`{name} <https://anaconda.org/{cloudchannel}/{name}/>`_".format(
                cloudchannel=cloudchannel,
                **pkg
            ),
            pkg['version'],
            pkg['build_string'],
            pkg['channel'],
        ))
    print(
        tabulate(
            rows,
            headers=('Name', 'Version', 'Build', 'Channel'),
            tablefmt="rst",
        ),
        file=file,
    )
