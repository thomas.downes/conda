#!/bin/bash

CONDA_CMD="conda env list"
if [ -z "${LSCSOFT_CONDA_DOCKER_IMAGE}" ]; then
    ${CONDA_CMD}
else
    docker run ${LSCSOFT_CONDA_DOCKER_IMAGE} ligo-py37 ${CONDA_CMD} | sed 's|/opt/conda|/cvmfs/ligo-containers.opensciencegrid.org/lscsoft/conda/latest|g'
fi
