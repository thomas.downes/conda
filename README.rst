*************
LSCSoft Conda
*************

|MIT license| |Build status|

.. |MIT license| image:: https://img.shields.io/badge/License-MIT-blue.svg
   :target: https://opensource.org/licenses/MIT
.. |Build status| image:: https://git.ligo.org/lscsoft/conda/badges/master/pipeline.svg
   :target: https://git.ligo.org/lscsoft/conda/commits/master

LSCSoft Conda is a programme to manage and distribute software
used by the LIGO Scientific Collaboration and partner groups
using the `conda <https://conda.io>`_ package manager, and the
`conda-forge <https://conda-forge.org>`_ community.

Quickstart
==========

**Use a pre-built environment (linux only):**

1. `Install and configure CVMFS <https://wiki.ligo.org/Computing/CvmfsUser>`_

2. Configure ``conda``:

   .. code-block:: bash

       source /cvmfs/ligo-containers.opensciencegrid.org/lscsoft/conda/latest/etc/profile.d/conda.sh

3. Activate an environment:

   .. code-block:: bash

       conda activate ligo-py37

For more details see `Reference environments <https://docs.ligo.org/lscsoft/conda/environments/>`_.

**Or install miniconda yourself:**

1. `Install Miniconda <https://conda.io/projects/conda/en/latest/user-guide/install/index.html>`_.

2. Add the conda-forge channel:

   .. code-block:: bash

      conda config --add channels conda-forge

3. Create an environment full of packages:

   .. code-block:: bash

      conda create --name myenv python=3.7 gwpy python-lal ...

For either solution, see `Tips and tricks <https://docs.ligo.org/lscsoft/conda/tips/>`_ for
more useful hints.

Contributing
============

If you would like to improve LSCSoft Conda, please consider one of the
following actions:

- `Report a problem <https://git.ligo.org/lscsoft/conda/issues/new?issuable_template=Bug>`_
- `Request a new/updated package <https://git.ligo.org/lscsoft/conda/issues/new?issuable_template=Request>`_
